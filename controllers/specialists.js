const Specialist = require('../models/Specialists');
const errorHandler = require('../utils/errorHandler');

//specialists(GET) требует доработки
module.exports.getAll = async function (req, res) {
    try {
        const specialists = await Specialist.find();
        // console.log(specialists);
        res.status(200).json(specialists);
    } catch (e) {
        errorHandler(res, e);
    };
};

//specialists/:id(GET) требует доработки
module.exports.getById = async function (req, res) {
    try {
        res.status(200).json({
            message: 'specialists/:id(GET) требует доработки'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};

//specialists(POST) требует доработки
module.exports.create = async function (req, res) {
    console.log(req.body)
    const specialist = new Specialist({
        name: req.body.name,
        nickName: req.body.nickName,
        user: req.user.id,
        photoSrc: req.file ? req.file.path : '',
        shortDescript: req.body.shortDescript,
        competencies: [],
        rating: []
    });
    try {
        await specialist.save();
        res.status(200).json(specialist);
    } catch (e) {
        errorHandler(res, e);
    };
};

//specialists/:id(DELETE) требует доработки
module.exports.remove = async function (req, res) {
    try {
        res.status(200).json({
            message: 'specialists/:id(DELETE) требует доработки'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};

//specialists/:id(PATCH) требует доработки
module.exports.update = async function (req, res) {
    try {
        res.status(200).json({
            message: 'specialists/:id(PATCH) требует доработки'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};

//specialists/skill/:id (GET) получение списка специалистов по навыку. требует доработки
module.exports.getSkill = async function (req, res) {
    try {
        res.status(200).json({
            message: 'specialists/skill/:id (GET) требует доработки'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};
