const Order = require('../models/Order');
const errorHandler = require('../utils/errorHandler');

//order(GET) требует доработки
module.exports.getAll = function(req, res) {
    try {

        res.status(200).json({
            message: 'order(GET) требует доработки'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};

//order(POST) требует доработки
module.exports.create = function(req, res) {
    try {

        res.status(200).json({
            message: 'order(POST) требует доработки'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};
