const Service = require('../models/Services');
const errorHandler = require('../utils/errorHandler');

//services(GET) запрос проверен
module.exports.getAll = async function(req, res) {
    try {
        const services = await Service.find();
        res.status(200).json(services);
    } catch (e) {
        errorHandler(res, e);
    };
};

//services/:id(GET) запрос проверен
module.exports.getById = async function(req, res) {
    try {
        const service = await Service.find({_id: req.params.id});
        res.status(200).json(service);
    } catch (e) {
        errorHandler(res, e);
    };
};

//services/:id(DELETE) запрос проверен
module.exports.remove = async function(req, res) {
    try {
        await Service.deleteOne({_id: req.params.id});
        res.status(200).json({
            message: 'услуга удалена из списка'
        });
    } catch (e) {
        errorHandler(res, e);
    };
};

//services(POST)  запрос проверен
module.exports.create = async function(req, res) {
    try {
        const service = await new Service({
            name: req.body.name
        }).save();
        res.status(201).json(service);
    } catch (e) {
        errorHandler(res, e);
    };
};

//services/:id(PATCH)  запрос проверен
module.exports.update = async function(req, res) {
    try {
        const service = await Service.findOneAndUpdate(
            {_id: req.params.id},
            {$set: req.body},
            {new: true}
            );
        res.status(200).json(service);
    } catch (e) {
        errorHandler(res, e);
    };
};
