//login(POST)
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const keys = require('../config/keys');
const errorHandler = require('../utils/errorHandler');


module.exports.login = async function(req, res) {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        //пользователь существует, проверка пароля
        const pasportResult =bcrypt.compareSync(req.body.password, candidate.password)
        if (pasportResult) {
            //Генерация токена, пароли совпали
            const token = jwt.sign({
                email: candidate.email,
                userId: candidate._id,
                userName: candidate.name
            }, keys.jwt, {expiresIn: 60 * 60});

            res.status(200).json({
                token: `Bearer ${token}`
            });
        } else {
            //Пароли не совпали
            res.status(401).json({
                message: 'ошибка ввода пароля. Попробуйте снова.'
            });
        }
    } else {
        //пользователь отсутствует в бызе
        res.status(404).json({
            message: 'Пользователь с таким email не найден'
        });
    }
};

//register(POST)
module.exports.register = async function(req, res) {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        //пользователь существует, отправить ошибку
        res.status(409).json({
            message: 'Такой email уже занят. Попробуйте указать другой'
        })
    } else {
        //записать нового пользователя в БД
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;

        const user = new User({
            email: req.body.email,
            password: bcrypt.hashSync(password, salt),
            name: req.body.name
        });
        try {
        await user.save();
        res.status(201).json(user);
        } catch (e) {
            //обработать ошибку
            errorHandler(res, e);
        }
    }
};