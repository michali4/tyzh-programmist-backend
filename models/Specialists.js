const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const specialistSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    nickName: {
        type: String,
        required: true,
        unique: true
    },
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId
    },
    photoSrc: {
        type: String,
        default: ''
    },
    shortDescript: {
        type: String,
        default: ''
    }
    // ,
    // competencies: [
    //     {
    //         skill: {
    //             ref: 'services',
    //             type: Schema.Types.ObjectId
    //         }
    //     }
    // ],
    // rating: [
    //     {
    //         customer: {
    //             type: String,
    //             required: true
    //         },
    //         assessment: {
    //             type: Number,
    //             required: true
    //         }
    //     }
    // ]
});

module.exports = mongoose.model('specialists', specialistSchema);