const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const skillSchema = new Schema({
    specialist: {
        ref: 'users',
        type: Schema.Types.ObjectId
    },
    skill: {
        ref: 'users',
        type: Schema.Types.ObjectId
    }
});

module.exports = mongoose.model('skills', skillSchema);