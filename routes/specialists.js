const express = require('express');
const pasport = require('passport');

const router = express.Router();
const upload = require ('../middleware/upload');
const controller = require('../controllers/specialists')

router.get('/skill/:id', pasport.authenticate('jwt', {session: false}), controller.getSkill);//одинаковые запросы? необходимо протестировать
router.get('/', pasport.authenticate('jwt', {session: false}), controller.getAll);
router.get('/:id', pasport.authenticate('jwt', {session: false}), controller.getById);//одинаковые запросы? необходимо протестировать
router.delete('/:id', pasport.authenticate('jwt', {session: false}), controller.remove);
router.post('/', pasport.authenticate('jwt', {session: false}), upload.single('image'), controller.create);
router.patch('/:id', pasport.authenticate('jwt', {session: false}), upload.single('image'), controller.update);



module.exports = router