const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const authRoutes = require('./routes/auth');
const orderRoutes = require('./routes/order');
const servicesRoutes = require('./routes/services');
const specialistsRoutes = require('./routes/specialists');
const keys = require('./config/keys')
const app = express();

// mongoose.set('useCreateIndex', true);
// mongoose.set('useNewUrlParser', true);
mongoose.connect(keys.mongoURI, {
        useNewUrlParser: true,
        useCreateIndex: true
    })
    .then(() => console.log('MongoDB connected.'))
    .catch(error => console.log(error));

app.use(passport.initialize());
require('./middleware/passport')(passport);

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());

app.use('/api/auth', authRoutes);
app.use('/api/order', orderRoutes);
app.use('/api/services', servicesRoutes);
app.use('/api/specialists', specialistsRoutes);


module.exports = app;









// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true }));

// let specialist = [
//     {
//       name: 'Василий',
//       nickName: 'vasya',
//       fhoto: 'photo/vasya.jpg',
//       costPerHour: 10,
//       shortDescrip: 'хороший специалист',
//       competencies: ['навык 1', 'навык 2', 'навык 3'],
//       rating: 0
//     },
//     {
//       name: 'Петр',
//       nickName: 'petya',
//       fhoto: 'photo/petya.jpg',
//       costPerHour: 12,
//       shortDescrip: 'хороший специалист',
//       competencies: ['навык 1', 'навык 3', 'навык 7', 'навык 8'],
//       rating: 0
//     },
//     {
//       name: 'Сергей',
//       nickName: 'sergei',
//       fhoto: 'photo/sergei.jpg',
//       costPerHour: 20,
//       shortDescrip: 'отличный специалист',
//       competencies: ['навык 1', 'навык 2', 'навык 3', 'навык 4', 'навык 5', 'навык 6'],
//       rating: 0
//     },
//     {
//       name: 'Андрей',
//       nickName: 'andrei',
//       fhoto: 'photo/andrei.jpg',
//       costPerHour: 5,
//       shortDescrip: 'начинающий специалист',
//       competencies: ['навык 1', 'навык 9'],
//       rating: 0
//     }
//   ];

// app.get('/', (req, res) => {
//     // res.send('Hello API');
//     res.status(200).json({
//       message: 'Working'
//     })
// });

// app.get('/specialist', (req, res) => {
//     res.send(specialist);
// });

// app.get('/specialist/:id', (req, res) => {
//     console.log(req.params);
//     var spec = specialist.find(function (spec) {
//         console.log(spec);
//         return spec.nickName === req.params.id;
//     });
//     res.send(spec);
// });

// app.post('/specialist', (req, res) => {
//   console.log('body =', req.body);
//   let spec = {
//     timeAdd: Date.now(),
//     name: req.body.name,
//     nickName: req.body.nickName,
//     fhoto: req.body.fhoto,
//     costPerHour: req.body.costPerHour,
//     shortDescrip: req.body.shortDescrip,
//     competencies: req.body.competencies,
//     rating: 0
//   };
//   specialist.push(spec);
//   console.log(spec);
//   res.send(spec);
// });